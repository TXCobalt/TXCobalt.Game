﻿using System;
using System.Linq;
using System.Threading;
using Pencil.Gaming;
using Pencil.Gaming.Graphics;
using TXCobalt.Core;
using System.IO;

namespace TXCobalt.Game
{
    /*
#if WINDOWS || LINUX
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            using (var game = new Game1())
                game.Run();
        }
    }
#endif
*/
    static class Program
    {
        public static GlfwWindowPtr window;

        [STAThread]
        static void Main()
        {
            //HACK: Fix dll error and some problems of current directory.
            Environment.CurrentDirectory = Path.GetDirectoryName(typeof(Program).Assembly.Location);

            Log.Write("TXCobalt.Game experimental version");
            Glfw.Init();
            GameSettings.current.SaveSettings();
            // Glfw.set

            Log.Write("Glfw version : {0}", Glfw.GetVersionString());

            window = Glfw.CreateWindow(1280, 1024, "TXCobalt.Game experimental", GlfwMonitorPtr.Null, GlfwWindowPtr.Null);
            Glfw.MakeContextCurrent(window);
            Glfw.ShowWindow(window);

            // Set 800x600 video mode
            DisplayManager.CurretVideoMode = DisplayManager.videomodes.First(mode => mode.Width == 1280 && mode.Height == 1024);

            GL.Viewport(0, 0, 1280, 1024);
            Glfw.SwapInterval(1);

            double mousex, mousey;


            Game game = new Game();
            Log.Write("Load assets...");
            game.Initialize();
            game.LoadAssets();

            Log.Write("Initialize Input Manager");
            InputManager.Init();

            // Do 100 updates per seconds
            Timer timer = new Timer(_ =>
            {
                try
                {
                    game.Update();
                }
                catch (Exception ex)
                {
                    ErrorScene.FastFail(ex);
                }
            }, null, 0, 10);

            #if !TRACE
            try
            #endif
            {
                while (!(Glfw.WindowShouldClose(window)))
                {
                    Glfw.PollEvents();
                    Glfw.GetCursorPos(Program.window, out mousex, out mousey);


                    GL.MatrixMode(MatrixMode.Projection);
                    GL.LoadIdentity();
                    GL.Ortho(1f, 0f, 1f, 0f, 0f, 1f);

                    GL.Clear(ClearBufferMask.ColorBufferBit);
                    GL.ClearColor(Color4.Black);

                    game.Draw();

                    Glfw.SwapBuffers(window);
                }
                game.Unload();
            }
            #if !TRACE
            catch (Exception e)
            {
                Log.Error("The game have just crashed :( : {0}\nStacktrace :\n{1}", e.Message, e.StackTrace);
                Glfw.SetCharCallback(Program.window, (wnd, ch) =>
                {
                });
                Glfw.SetKeyCallback(Program.window, (wnd, key, scanCode, action, mods) =>
                {
                });

                ErrorScene.FastFail(e);
                while (true)
                {
                    Glfw.PollEvents();
                    GL.MatrixMode(MatrixMode.Projection);
                    GL.LoadIdentity();
                    GL.Ortho(1f, 0f, 1f, 0f, 0f, 1f);
                    GL.Clear(ClearBufferMask.ColorBufferBit);
                    GL.ClearColor(Color4.Black);
                    Scene.DrawScene();
                    Glfw.SwapBuffers(window);
                }
            }
            #endif
            Glfw.Terminate();
        }
    }
}