﻿using Pencil.Gaming.Graphics;

namespace TXCobalt.Game.UI
{
    public struct ColoredString
    {
        public ColoredString(string text)
        {
            Text = text;
            Color = Color4.White;
        }
        public ColoredString(string text, Color4 color)
        {
            Text = text;
            Color = color;
        }

        public string Text;
        public Color4 Color;
    }
}
