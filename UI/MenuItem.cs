﻿/*
 *  This file is a part of TXCobalt MonoGame client.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace TXCobalt.Game.UI
{
    /// <summary>
    /// Base class for menu item.
    /// </summary>
    public class MenuItem : IDisposable
    {
        public MenuItem() { }

        public MenuItem(string text)
        {
            Text = text;
            action = _ => { };
            Update = () => text;
            NeedUpdate = false;
            
        }

        public MenuItem(string text, Action<MenuKeyMode> action)
        {
            this.action = action;
            Text = text;
            Update = () => text;
            NeedUpdate = false;
        }
        public MenuItem(string text, Action<MenuKeyMode> action, Func<string> update)
        {
            this.action = action;
            Text = text;
            Update = update;
            NeedUpdate = true;
        }

        public virtual void Dispose()
        {
        }

        public string Text;
        /// <summary>
        /// Action to do when a special key is pressed when the item is focused.
        /// </summary>
        public Action<MenuKeyMode> action;
        /// <summary>
        /// Update called once per Update() only when NeedUpdate is true.
        /// </summary>
        public Func<string> Update;
        /// <summary>
        /// If Text is updated using Update Func.
        /// </summary>
        public bool NeedUpdate;
        /// <summary>
        /// If is selected in menu.
        /// </summary>
        public bool Focused;
        /// <summary>
        /// Allow one action each Update().
        /// </summary>
        public bool AllowKeySpam = false;
    }
}
