﻿/*
 *  This file is a part of TXCobalt MonoGame client.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;

namespace TXCobalt.Game.UI
{
    /// <summary>
    /// Boolean switch item.
    /// </summary>
    public class MenuSwitch : MenuItem
    {
        public bool Value, DrawArrow;

        public MenuSwitch(string text, bool Value, bool drawarrows) : base(text)
        {
            this.Value = Value;
            action = _ => this.Value = !this.Value;
            Update = () =>
            {
                string value = "";

                if (DrawArrow && Focused)
                    value += "< ";

                value += text + " : " + this.Value;

                if (DrawArrow && Focused)
                    value += " >";

                return value;
            };
            NeedUpdate = true;
            DrawArrow = drawarrows;
        }
        public MenuSwitch(string text, Func<bool> getter, Action<bool> setter, bool drawarrows) : base(text)
        {
            action = _ => setter(!getter());
            Update = () =>
            {
                string value = "";

                if (DrawArrow && Focused)
                    value += "< ";

                value += text + " : " + getter();

                if (DrawArrow && Focused)
                    value += " >";

                return value;
            };
            NeedUpdate = true;
            DrawArrow = drawarrows;
        }
    }
}