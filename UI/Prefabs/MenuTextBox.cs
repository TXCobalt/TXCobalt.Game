﻿/*
 *  This file is a part of TXCobalt MonoGame client.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using Pencil.Gaming;

namespace TXCobalt.Game.UI
{
    /// <summary>
    /// Textbox item.
    /// </summary>
    public class MenuTextBox : MenuItem
    {
        readonly Action<MenuKeyMode> Action;

        bool BoxFocused;
        /// <summary>
        /// Is the TextBox using Getter and Setter delegates ?
        /// </summary>
        bool PropertyMode;
        string _value;

        Func<string> Getter;
        Action<string> Setter;

        readonly Menu Menu;

        public string Value
        {
            get
            {
                return PropertyMode ? Getter() : _value;
            }
            set
            {
                if (PropertyMode)
                    Setter(value);
                else
                    _value = value;
            }
        }

        public event Action<char> CharPressed = obj => {};
        public event Action<Key, KeyAction, KeyModifiers> KeyPressed = (key, action, modifiers) => {};
        public event Action Selected = () => {};
        public event Action Disposed = () => {};

        MenuTextBox(Menu menu, string text)
        {
            Menu = menu;
            Text = text;
           
            #region Delegates
            menu.CharPressed += Char =>
            {
                if (BoxFocused && Focused)
                {
                    CharPressed(Char);
                    Value += Char;
                }
            };
            menu.KeyPressed += (key, action, modifiers) =>
            {
                if (BoxFocused && Focused)
                {
                    KeyPressed(key, action, modifiers);

                    if (BoxFocused && action == KeyAction.Press || BoxFocused && action == KeyAction.Repeat)
                    {
                        if (key == Key.Backspace && Value.Length > 0)
                            Value = Value.Substring(0, Value.Length - 1);
                    }
                }
            };
            Action = key =>
            {
                if (key == MenuKeyMode.Enter)
                {
                    BoxFocused = menu.LockIndex = !BoxFocused;
                    Selected();
                }
            };
            #endregion

            Update = () => string.Format("{0} : {1}", text, Value);
            action = Action;
            NeedUpdate = true;
        }

        public MenuTextBox(string text, Menu menu, string baseValue)
            : this(menu, text)
        {
            PropertyMode = false;
            _value = baseValue;

            Value = baseValue;
        }

        public MenuTextBox(string text, Menu menu, Func<string> getter, Action<string> setter)
            : this(menu, text)
        {
            PropertyMode = true;

            Getter = getter;
            Setter = setter;
        }

        public override void Dispose()
        {
            Disposed();
        }
    }
}
