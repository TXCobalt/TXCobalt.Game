﻿using System;
using System.Collections.Generic;
using Pencil.Gaming;

namespace TXCobalt.Game.UI
{
    public class KeyboardText
    {
        public KeyboardText()
        {
            InputManager.AddFunction((wnd, Char) =>
            {
                if(Active)
                    chars.Add(Char);
            });
        }

        float AnimState = 0f;

        List<char> chars = new List<char>();

        bool Active = true;

        public void Update()
        {
            AnimState += (float)Glfw.GetTime();
        }
    }
}
