﻿using System;
using System.Linq;
using TXCobalt.Core;

namespace TXCobalt.Game.UI
{
    public class BasicConsole
    {
        public BasicConsole(DevConsole console)
        {
            this.console = console;
            console.OnPrompt += OnPrompt;
        }

        DevConsole console;

        void OnPrompt(string message)
        {
            string[] parts = message.Split(' ');

            switch (parts[0].ToLower())
            {
                case "clear":
                    console.log.Clear();
                    break;
                case "exit":
                    Environment.Exit(0);
                    break;
                case "print":
                    Log.Write(string.Join(" ", parts.Skip(1)));
                    break;
                case "help":

                    break;
                default:
                    Log.Write(string.Format("Unknow command {0}", parts[0]));
                    break;
            }
        }
    }
}

