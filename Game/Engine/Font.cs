﻿using QuickFont;

namespace TXCobalt.Game
{
    public class Font
    {
        public QFont font;

        public Font(string path, bool Fancy, float size = 12f)
        {
            bool shadow = Fancy;
            var config = new QFontBuilderConfiguration(shadow);
            config.SuperSampleLevels = Fancy ? 8 : 4;
            font = new QFont(path, size, System.Drawing.FontStyle.Regular, config);
        }
    }
}

