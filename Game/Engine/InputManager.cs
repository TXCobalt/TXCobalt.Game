﻿using System;
using Pencil.Gaming;
using System.Collections.Generic;
using System.Collections;

namespace TXCobalt.Game
{
    public static class InputManager
    {
        static List<GlfwKeyFun>[] keydelegates = new List<GlfwKeyFun>[1];
        static List<GlfwCharFun>[] chardelegates = new List<GlfwCharFun>[1];

        /// <summary>
        /// Channel used to separe the callbacks.
        /// </summary>
        static int curretChannel;

        public static void Init()
        {
            Glfw.SetCharCallback(Program.window, CallbackChar);
            Glfw.SetKeyCallback(Program.window, CallbackKey);
        }

        static void CallbackKey(GlfwWindowPtr wnd, Key key, int scanCode, KeyAction action, KeyModifiers mods)
        {
            CheckChannel(curretChannel);

            List<GlfwKeyFun> channel = keydelegates[curretChannel];
            var callbacks = channel.ToArray();
            Array.ForEach(callbacks, d => d(wnd, key, scanCode, action, mods));
        }

        static void CallbackChar(GlfwWindowPtr wnd, char ch)
        {
            CheckChannel(curretChannel);

            List<GlfwCharFun> channel = chardelegates[curretChannel];
            var callbacks = channel.ToArray();
            Array.ForEach(callbacks, d => d(wnd, ch));
        }

        // Add
        public static void AddFunction(GlfwKeyFun function, int channel = 0)
        {
            CheckChannel(channel);
            keydelegates[channel].Add(function);
        }

        public static void AddFunction(GlfwCharFun function, int channel = 0)
        {
            CheckChannel(channel);

            chardelegates[channel].Add(function);
        }

        // Remove
        public static void RemoveFunction(GlfwKeyFun function, int channel = 0)
        {
            if (keydelegates.Length <= channel || keydelegates[curretChannel] == null)
                keydelegates[channel].Remove(function);
        }

        public static void RemoveFunction(GlfwCharFun function, int channel = 0)
        {
            if (chardelegates.Length <= channel || chardelegates[curretChannel] == null)
                chardelegates[channel].Remove(function);
        }

        // Clear
        public static void ClearCharCallback(int channel = 0)
        {
            if (chardelegates.Length <= channel || chardelegates[curretChannel] == null)
                chardelegates[channel] = null;
        }

        public static void ClearKeyCallback(int channel = 0)
        {
            if (keydelegates.Length <= channel || keydelegates[curretChannel] == null)
                keydelegates[channel] = null;
        }

        public static void SwitchChannel(int channel)
        {
            curretChannel = channel;
        }
        static void CheckChannel(int channel)
        {
            if (keydelegates.Length <= channel)
                Array.Resize(ref keydelegates, channel + 1);
            if (chardelegates.Length <= channel)
                Array.Resize(ref chardelegates, channel + 1);

            if(keydelegates[channel] == null)
                keydelegates[channel] = new List<GlfwKeyFun>();
            if(chardelegates[channel] == null)
                chardelegates[channel] = new List<GlfwCharFun>();
        }
        public static int GetFreeChannel()
        {
            for (int i = 0; i < Math.Max(keydelegates.Length, chardelegates.Length); i++)
                if(keydelegates.Length > i && chardelegates.Length > i && (keydelegates[i] == null || keydelegates[i].Count == 0) && (chardelegates[i] == null || chardelegates[i].Count == 0))
                    return i;

            return Math.Max(keydelegates.Length, chardelegates.Length) + 1;
        }
    }
}

