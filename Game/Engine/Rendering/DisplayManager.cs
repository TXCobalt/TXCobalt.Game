﻿using Pencil.Gaming;
using Pencil.Gaming.MathUtils;
using TXCobalt.Core;

namespace TXCobalt.Game
{
    public static class DisplayManager
    {
        static DisplayManager()
        {
            videomodes = Glfw.GetVideoModes(Glfw.GetPrimaryMonitor());
            Log.Write("TXCobalt.Game display manager loaded.");
            Log.Write("Found {0} resolutions available", videomodes.Length);
            foreach (var videomode in videomodes)
                Log.Write("  * {0}x{1}*{2} {3}hz mode found.", videomode.Width, videomode.Height, videomode.RedBits + videomode.BlueBits + videomode.GreenBits, videomode.RefreshRate);

            monitor = GlfwMonitorPtr.Null;
            CurretVideoMode = videomodes[videomodes.Length - 3];
        }

        public static GlfwVidMode[] videomodes;
        public static GlfwVidMode CurretVideoMode;
        static GlfwMonitorPtr monitor;

        public static bool Fullscreen
        {
            get
            {
                return monitor.Equals(Glfw.GetPrimaryMonitor());
            }
            private set
            {
                if (value && !Fullscreen)
                {
                    monitor = Glfw.GetPrimaryMonitor();
                    Glfw.DestroyWindow(Program.window);
                }
                else
                    monitor = GlfwMonitorPtr.Null;

                Program.window = Glfw.CreateWindow(CurretVideoMode.Width, CurretVideoMode.Height, "TXCobalt Game Experimental", monitor, GlfwWindowPtr.Null);

            }
        }
        public static Rectanglei viewport
        {
            get
            {
                return new Rectanglei(0, 0, CurretVideoMode.Width, CurretVideoMode.Height);
            }
        }
    }
}

