﻿using System.Drawing;
using Pencil.Gaming.MathUtils;
using Pencil.Gaming.Graphics;

namespace TXCobalt.Game
{
    public class Texture
    {
        public readonly Image texture_image;
        public Rectanglei Bounds
        {
            get
            {
                return new Rectanglei(0, 0, texture_image.Width, texture_image.Height);
            }
        }

        public Texture(string path)
        {
            texture_image = Image.FromFile(path);

        }
        public Texture(Bitmap bitmap)
        {
            texture_image = bitmap;
        }

        public static Texture CreateEmpty(int Width, int Height)
        {
            Bitmap bitmap = new Bitmap(Width, Height);
            for (int x = 0; x < Width; x++)
                for (int y = 0; y < Height; y++)
                    bitmap.SetPixel(Width, Height, Color.Transparent);

            return new Texture(bitmap);
        }

        public int GenTexture()
        {
            return GL.Utils.LoadImage((Bitmap)texture_image);
        }
    }
}

