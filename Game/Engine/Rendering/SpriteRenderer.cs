﻿using System;
using Pencil.Gaming.Graphics;
using Pencil.Gaming.MathUtils;

namespace TXCobalt.Game
{
    public static class SpriteRenderer
    {
        public static void Draw(Texture texture, Vector2 bound1, Vector2 bound2, float rotation = 0, Color4? color = null)
        {
            if (!color.HasValue)
                color = Color4.White;

            // Don't draw useless things.
            if (bound1.X < 0 && bound1.Y < 0 && bound2.X < 0 && bound2.Y < 0)
                return;
            
            rotation = MathHelper.ToRadians(rotation);

            Rectangle rect = new Rectangle(bound1, bound2 - bound1);

            var p1 = new Vector2(rect.Left, rect.Top); 
            var p2 = new Vector2(rect.Right, rect.Top); 
            var p3 = new Vector2(rect.Right, rect.Bottom); 
            var p4 = new Vector2(rect.Left, rect.Bottom); 


            var o = div(bound2 + bound1, new Vector2(2f, 2f));

            rotate(ref p1, o, rotation); rotate(ref p2, o, rotation);
            rotate(ref p3, o, rotation); rotate(ref p4, o, rotation);

            Vector2 format = new Vector2(DisplayManager.viewport.Width, DisplayManager.viewport.Height);


            int tex = texture.GenTexture();

            GL.BindTexture(TextureTarget.Texture2D, tex);
            GL.Enable(EnableCap.Texture2D);

            GL.Begin(BeginMode.Quads);
            GL.Color4(color.Value);
            GL.TexCoord2(0f, 1f);
            GL.Vertex2(div(p1,format));
            GL.TexCoord2(1f,1f);
            GL.Vertex2(div(p2,format));
            GL.TexCoord2(1f, 0f);
            GL.Vertex2(div(p3,format));
            GL.TexCoord2(0f, 0f);
            GL.Vertex2(div(p4,format));
            GL.End();

            GL.DeleteTexture(tex);
        }

        /// <summary>
        /// Draws an rectangle.
        /// </summary>
        /// <param name="bound1">First bound position of the rectangle.</param>
        /// <param name="bound2">Last bound position of the rectangle.</param>
        /// <param name="rotation">Rotation of the square in degrees.</param>
        /// <param name="color">Blending color</param>
        public static void DrawRectangle(Vector2 bound1, Vector2 bound2, float rotation, Color4 color)
        {
            GL.Disable(EnableCap.Texture2D);
            rotation = MathHelper.ToRadians(rotation);

            // Don't draw useless squares.
            if (bound1.X < 0 && bound1.Y < 0 && bound2.X < 0 && bound2.Y < 0)
                return;

            Rectangle rect = new Rectangle(bound1, bound2 - bound1);

            var p1 = new Vector2(rect.Left, rect.Top); 
            var p2 = new Vector2(rect.Right, rect.Top); 
            var p3 = new Vector2(rect.Right, rect.Bottom); 
            var p4 = new Vector2(rect.Left, rect.Bottom); 


            var o = div(bound2 + bound1, new Vector2(2f, 2f));

            rotate(ref p1, o, rotation); rotate(ref p2, o, rotation);
            rotate(ref p3, o, rotation); rotate(ref p4, o, rotation);

            Vector2 format = new Vector2(DisplayManager.viewport.Width, DisplayManager.viewport.Height);

            GL.Begin(BeginMode.Quads);
            GL.Color4(color);
            GL.Vertex2(div(p1,format));
            GL.Vertex2(div(p2,format));
            GL.Vertex2(div(p3,format));
            GL.Vertex2(div(p4,format));
            GL.End();
        }
        static Vector2 div(Vector2 a, Vector2 b)
        {
            return new Vector2(a.X / b.X, a.Y / b.Y);
        }
        static void rotate(ref Vector2 a, Vector2 o, float theta)
        {
            float cos = (float)Math.Cos(theta);
            float sin = (float)Math.Sin(theta);
            float px = cos * (a.X - o.X) - sin * (a.Y - o.Y) + o.X;
            float py = sin * (a.X - o.X) + cos * (a.Y - o.Y) + o.Y;
            a = new Vector2(px, py);
        }
    }
}

