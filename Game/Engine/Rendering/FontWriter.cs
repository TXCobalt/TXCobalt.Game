﻿using Pencil.Gaming.Graphics;
using Pencil.Gaming.MathUtils;
using System.Drawing;
using QuickFont;

namespace TXCobalt.Game
{
    public static class FontWriter
    {
        public static void DrawString(string text, Font font, Vector2 position, Color4 color)
        {
            font.font.PushOptions(new QFontRenderOptions { Colour = color });

            QFont.Begin();
            font.font.Print(text, position);
            QFont.End();
        }
        public static Vector2 MesureString(string text, Font font)
        {
            SizeF ssize = font.font.Measure(text);
            return new Vector2(ssize.Width, ssize.Height);
        }
    }
}

