﻿/*
 *  This file is a part of TXCobalt MonoGame client.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using TXCobalt.Core;

namespace TXCobalt.Game
{
    /// <summary>
    /// Provider used for communication between this and a local GameInstance.
    /// </summary>
    public class LocalProvider : DataProvider
    {
        GameInstanceManager InstanceManager;
        Guid PlayerGUID;

        public LocalProvider(GameInstanceManager instanceManager)
        {
            InstanceManager = instanceManager;
        }

        public override void Dispose()
        {
            Log.Write("Kill server.");
            InstanceManager.Dispose();
        }

        public override ServerResponse GetResponse()
        {
            return new ServerResponse();
        }

        public override GameUpdate GetUpdate()
        {
            try
            {
                return ((LocalPlayer)InstanceManager.GetPlayer(PlayerGUID)).Update;
            }
            catch
            {
                return new GameUpdate { CameraPostion = new Vector2D(0, 0), gameobjects = new GameObjectContainer[] { }, Players = new PlayerData[] { } };
            }
        }

        public override void SendData(InputData input)
        {
            ((LocalPlayer)InstanceManager.GetPlayer(PlayerGUID)).Input = input;
        }

        public override void SendRequest(ConnectionRequest request)
        {
            Guid? guid = InstanceManager.SendConnectionRequest(request);

            if (!guid.HasValue)
                throw new Exception("Unable to request the local server.");
            PlayerGUID = guid.GetValueOrDefault();
        }
    }
}
