﻿/*
 *  This file is a part of TXCobalt MonoGame client.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using TXCobalt.Core;
using Pencil.Gaming.Graphics;

namespace TXCobalt.Game
{
    /// <summary>
    /// A scene is like a world but managed like the Game class.
    /// </summary>
    public abstract class Scene : IDisposable
    {
        // Class fields
        public Color4 BackgroundColor = new Color4(153, 204, 255, 255);

        // Static fields
        public static int CurretSceneID = 0;
        public static Dictionary<int,Scene> scenes = new Dictionary<int, Scene>();


        // Class fonctions
        public virtual void Initialize() { }
        public virtual void LoadContent() { }
        public virtual void Draw()
        {
            GL.ClearColor(BackgroundColor);
            GL.Clear(ClearBufferMask.DepthBufferBit);
            // spriteBatch.GraphicsDevice.Clear(BackgroundColor);
        }
        public virtual void Update() { }

        // Static fonctions
        public static void AddScene(Scene scene, int ID)
        {
            scenes.Add(ID, scene);
        }
        public static void Unload(int ID)
        {
            scenes[ID].Dispose();
            scenes.Remove(ID);
        }
        public static void UpdateScene()
        {
            Scene s;
            if (scenes.TryGetValue(CurretSceneID, out s))
                s.Update();
            else
                throw new Exception();
        }
        public static void DrawScene()
        {
            Scene s;
            if (scenes.TryGetValue(CurretSceneID, out s))
                s.Draw();
            else if (CurretSceneID != -1)
            {
                CurretSceneID = -1;
                Log.Error("Unknow Scene:{0}", CurretSceneID);
            }
        }
        public static void ChangeScene(int ID)
        {
            CurretSceneID = ID;
        }
        public static void LoadScene(int ID)
        {
            CurretSceneID = ID;
            scenes[ID].Initialize();
            scenes[ID].LoadContent();
        }

        public abstract void Dispose();
    }
}
