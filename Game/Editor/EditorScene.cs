﻿using System;
using TXCobalt.Core;
using Pencil.Gaming;
using Pencil.Gaming.MathUtils;
using Pencil.Gaming.Graphics;
using TXCobalt.Game;

namespace TXCobalt.Game.Editor
{
    class EditorScene : Scene
    {
        public EditorScene(Map? map = null)
        {
            world = new World();


            this.map = !map.HasValue ? new Map(new byte[128 * 128], 128, 128, new Vector2D(5, 5)) : map.Value;
        }

        Map map;

        World world;
        string mapname = "export";

        public override void Initialize()
        {

        }

        public override void LoadContent()
        {
        }

        public override void Update()
        {
            double mouseX, mouseY;
            Glfw.GetCursorPos(Program.window, out mouseX, out mouseY);

            int tilex = (int)((mouseX - (int)world.CameraPosition.X) / GameSettings.current.TileSize + 1);
            int tiley = (int)((mouseY - (int)world.CameraPosition.Y) / GameSettings.current.TileSize);

            if (!(tilex < 0 || tiley < 0 || tilex > map.Width || tiley > map.Height))
                if (Glfw.GetMouseButton(Program.window, MouseButton.Button1))
                    map.MapData[tiley * map.Width + tilex] = 128;
                else if (Glfw.GetMouseButton(Program.window, MouseButton.Button2))
                    map.MapData[tiley * map.Width + tilex] = 0;

            if (Glfw.GetKey(Program.window, Key.Left))
                world.TranslateCamera(new Vector2(GameSettings.current.TileSize, 0));
            if (Glfw.GetKey(Program.window, Key.Right))
                world.TranslateCamera(new Vector2(-GameSettings.current.TileSize, 0));
            if (Glfw.GetKey(Program.window, Key.Up))
                world.TranslateCamera(new Vector2(0, GameSettings.current.TileSize));
            if (Glfw.GetKey(Program.window, Key.Down))
                world.TranslateCamera(new Vector2(0, -GameSettings.current.TileSize));

            if (Glfw.GetKey(Program.window, Key.S))
                map.ExportMap(mapname + ".map");

            world.Update();
        }

        public override void Draw()
        {
            base.Draw();

            double mousex, mousey;
            Glfw.GetCursorPos(Program.window, out mousex, out mousey);

            int tilex = (int)((mousex - world.CameraPosition.X) / GameSettings.current.TileSize + 1);
            int tiley = (int)((mousey - world.CameraPosition.Y) / GameSettings.current.TileSize);

            #region Draw map
            for (int y = 0; y < map.Height; y++)
                for (int x = 0; x < map.Width; x++)
                {
                    int tileid = map.MapData[y * map.Width + x] % 127;
                    if (tileid != 0)
                    {
                        Texture texture;
                        if (!Game.content.MapTexture.TryGetValue(tileid, out texture))
                        {
                            texture = Texture.CreateEmpty(GameSettings.current.TileSize, GameSettings.current.TileSize);
                            Console.WriteLine("Unknow map texture " + map.DataArray[x, y]);
                        }
                        var drawingrect = new Rectangle(
                                              GameSettings.current.resX - (x * GameSettings.current.TileSize + (int)world.CameraPosition.X),
                                              y * GameSettings.current.TileSize + (int)world.CameraPosition.Y,
                                              GameSettings.current.TileSize, GameSettings.current.TileSize
                        );
                        SpriteRenderer.Draw(texture, new Vector2(drawingrect.X, drawingrect.Y), new Vector2(drawingrect.X + drawingrect.Width, drawingrect.Y + drawingrect.Height));
                    }
                }

            #endregion
            var selectedcolor = Color4.Green;

            // Check if the tile is editable.
            if (tilex < 0 || tiley < 0 || tilex > map.Width || tiley > map.Height)
                selectedcolor = Color4.Red;
            else
                try
                {
                    map.MapData[tiley * map.Width + tilex] = map.MapData[tiley * map.Width + tilex];
                }
                catch
                {
                    selectedcolor = Color4.Red;
                }

            var pos = new Vector2(GameSettings.current.resX - (tilex * GameSettings.current.TileSize + (int)world.CameraPosition.X), 
                tiley * GameSettings.current.TileSize + (int)world.CameraPosition.Y);

            if (Game.content.OtherTextures.ContainsKey("Editor_Selected"))
                SpriteRenderer.Draw(Game.content.OtherTextures["Editor_Selected"], pos, pos + new Vector2(-GameSettings.current.TileSize, GameSettings.current.TileSize), 0, selectedcolor);
            else
                Log.Warning("Texture not found : Editor_Selected");
        }

        public override void Dispose()
        {
        }
    }
}