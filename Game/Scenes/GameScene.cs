﻿/*
 *  This file is a part of TXCobalt MonoGame client.
 *  Copyright (C) 2015 Astie Teddy
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

using System;
using System.Collections.Generic;
using System.Threading;

using TXCobalt.Core;
using Pencil.Gaming;
using Pencil.Gaming.MathUtils;
using Pencil.Gaming.Graphics;

namespace TXCobalt.Game
{
    public class GameScene : Scene
    {
        public GameScene(DataProvider provider, ConnectionRequest request, bool internalServer = false)
        {
            world = new World();

            gameContent = Game.content;
            dataProvider = provider;

            serverResponse = provider.GetResponse();

            inputData = new InputData();

            Log.Write("Server map: {0}", serverResponse.Map);

            provider.SendRequest(request);

            lastUpdate = new GameUpdate { gameobjects = new GameObjectContainer[0], Players = new PlayerData[0], CameraPostion = new Vector2D(0, 0), playerObject = new PlayerObjectData { argbcolor = 0, Health = -1 } };

            map = Map.ImportMap("Maps\\" + GameSettings.current.Map + ".map");
            InternalServer = internalServer;
        }

        GameContent gameContent;

        /// <summary>
        /// Task used for communication beetween Game and dataProvider.
        /// </summary>
        Thread ExangeThread;

        DataProvider dataProvider;
        World world;

        Map map;
        InputData inputData;
        ServerResponse serverResponse;

        GameUpdate lastUpdate;
        bool InternalServer;
        bool HasKill = false;

        public override void Initialize()
        {
            ExangeThread = new Thread(() =>
            {
                while (true)
                {
                    lastUpdate = dataProvider.GetUpdate();
                    dataProvider.SendData(inputData);
                    if (!InternalServer)
                        inputData.IsMove = inputData.IsFire = false;
                    Thread.Sleep(50);
                }
            });
        }

        public override void Update()
        {
            #region TXCobalt movements
            bool IsMove = false;
            Tilt direction = inputData.Direction;

            if (Glfw.GetKey(Program.window, Key.Down))
            {
                direction = Tilt.Down;
                IsMove = true;
            }
            if (Glfw.GetKey(Program.window, Key.Up))
            {
                direction = Tilt.Up;
                IsMove = true;
            }
            if (Glfw.GetKey(Program.window, Key.Left))
            {
                direction = Tilt.Left;
                IsMove = true;
            }
            if (Glfw.GetKey(Program.window, Key.Right))
            {
                direction = Tilt.Right;
                IsMove = true;
            }

            if (Glfw.GetKey(Program.window, Key.Escape) && !HasKill)
            {
                HasKill = true;
                Unload(1);
                // Go to menu
                ChangeScene(0);
            }

            inputData = new InputData { Direction = direction, IsFire = Glfw.GetKey(Program.window, Key.Space), IsMove = IsMove };
            #endregion
            world.Update();
            world.UpdateEntities(lastUpdate.gameobjects);

            // Move smoothly camera at the player positon.
            world.camera.SmoothMoveTo(new Vector2(
                GameSettings.current.resX - lastUpdate.CameraPostion.x * GameSettings.current.TileSize,
                GameSettings.current.resY - lastUpdate.CameraPostion.y * GameSettings.current.TileSize
            ));
        }

        public override void LoadContent()
        {
            ExangeThread.Start();
        }

        public override void Draw()
        {
            base.Draw();
            #region Draw map
            for (int x = 0; x < map.Width; x++)
                for (int y = 0; y < map.Height; y++)
                {
                    int tileid = map.MapData[y * map.Width + x] % 127;
                    if (tileid != 0)
                    {            
                        //FIXME: Wrong positions
                        var drawingrect = new Rectangle(
                            GameSettings.current.resX - (x * GameSettings.current.TileSize + world.CameraPosition.X - GameSettings.current.resX / 2),
                            y * GameSettings.current.TileSize + world.CameraPosition.Y - GameSettings.current.resY / 2,
                            GameSettings.current.TileSize, GameSettings.current.TileSize
                        );
                        
                        Texture texture;
                        if (!gameContent.MapTexture.TryGetValue(tileid, out texture))
                        {
                            SpriteRenderer.DrawRectangle(new Vector2(drawingrect.Left, drawingrect.Bottom), new Vector2(drawingrect.Right, drawingrect.Top), 0, Color4.DarkMagenta);
                            Log.Error("Unknow texture {0}", tileid);
                        }
                        else
                            SpriteRenderer.Draw(texture, new Vector2(drawingrect.Left, drawingrect.Bottom), new Vector2(drawingrect.Right, drawingrect.Top));
                    }
                }
            #endregion

            #region Draw Entites
            foreach (KeyValuePair<Guid, Entity> entitydata in world.Entities)
                entitydata.Value.Draw(gameContent, world);
            #endregion

            // Draw health
            Color4 c = Color4.White;
            if (lastUpdate.playerObject.TookDamage)
                c = Color4.Red;

            FontWriter.DrawString("Health : " + lastUpdate.playerObject.Health, Game.content.Fonts["menu_font"], new Vector2(10, 10), c);
        }

        public override void Dispose()
        {
            ExangeThread.Abort();
            dataProvider.Dispose();
            // MediaPlayer.Stop();
        }
    }
}
