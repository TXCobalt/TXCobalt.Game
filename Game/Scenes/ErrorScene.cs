﻿using System;
using Pencil.Gaming.Graphics;
using Pencil.Gaming.MathUtils;
using System.Linq;

namespace TXCobalt.Game
{
    public class ErrorScene : Scene
    {
        Exception exception;
        Font font;

        public ErrorScene(Exception e)
        {
            for (int i = 0; i < Scene.scenes.Count; i++)
                try
                {
                    Scene.Unload(Scene.scenes.Keys.ToArray()[i]);
                    i--;
                }
                catch
                {
                }
            exception = e;

            font = Game.content.Fonts["console_font"];
        }

        public static void FastFail(Exception e)
        {
            Scene.AddScene(new ErrorScene(e), 5);
            Scene.LoadScene(5);
        }

        public override void Draw()
        {
            FontWriter.DrawString(string.Format(":( TXCobalt have crashed : {0}", exception.Message), font, new Vector2(10, 10), Color4.OrangeRed);
            if (exception.StackTrace != null)
            {
                FontWriter.DrawString("Stacktrace (Give this to the developers, to help correct this problem) : ", font, new Vector2(10, 50), Color4.Gold);
                FontWriter.DrawString(exception.StackTrace, font, new Vector2(10, 75), Color4.White);
            }
        }
        public override void Dispose()
        {
            
        }
    }
}

